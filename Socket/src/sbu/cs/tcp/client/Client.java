package sbu.cs.tcp.client;

import java.io.*;
import java.net.*;

public class Client {
	private final String address;
	private final int port;
	private Socket s;
	private BufferedReader in;
	private PrintWriter out;
	private PrintStream ps;

	public Client(String address, int port, PrintStream ps) throws UnknownHostException, IOException {
		this.address = address;
		this.port = port;
		this.ps = ps;

		ps.println("Trying to connect...");
		s = new Socket(address, port);
		in = new BufferedReader(new InputStreamReader(s.getInputStream()));
		out = new PrintWriter(s.getOutputStream(), true);
		ps.println("Connection stablished");
	}

	public String listen() throws IOException {
		ps.println("Waiting for server message...");
		String message = in.readLine();
		ps.println("Message recived: " + message);
		return message;
	}

	public void send(String message) {
		out.println(message);
		ps.println("Message sent: " + message);
	}

	public void close() throws IOException {
		s.close();
	}

}
