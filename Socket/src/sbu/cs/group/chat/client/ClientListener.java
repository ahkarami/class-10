package sbu.cs.group.chat.client;

import sbu.cs.group.chat.server.Message;

import java.io.IOException;
import java.io.InputStream;

public class ClientListener implements Runnable{
    private InputStream in;
    private Thread thread;

    public ClientListener(InputStream in) {
        this.in = in;
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        byte[] buffer = new byte[256];
        while (true){
            int len = 0;
            try {
                len = in.read(buffer);
                byte[] tmp = new byte[len];
                System.arraycopy(buffer, 0, tmp, 0, len);
                String message = new String(tmp);
                System.out.println(message);
                buffer = new byte[256];
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
