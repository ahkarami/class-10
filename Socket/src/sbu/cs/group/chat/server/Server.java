package sbu.cs.group.chat.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;

public class Server implements Runnable {
    private ArrayList<ClientStruct> clients;
    private ArrayBlockingQueue<Message> messages;

    public Server(ArrayList<ClientStruct> clients
            , ArrayBlockingQueue<Message> messages) {
        this.clients = clients;
        this.messages = messages;
    }

    @Override
    public void run() {
        while (true){
            try {
                broadcast(messages.take());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void broadcast(Message message){
        for (ClientStruct client : clients){
            if (client != message.getSender()){
                try {
                    client.sendMessage(message.getSender().getId()
                            + " : " + message.getData());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
