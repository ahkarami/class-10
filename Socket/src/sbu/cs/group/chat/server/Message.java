package sbu.cs.group.chat.server;

public class Message {
    private String data;
    private ClientStruct sender;

    public Message(String data, ClientStruct sender) {
        this.data = data;
        this.sender = sender;
    }

    public String getData() {
        return data;
    }

    public ClientStruct getSender() {
        return sender;
    }
}
