package sbu.cs.group.chat.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;

public class ServerListener {
    public static void main(String[] args){
        ArrayList<ClientStruct> clients = new ArrayList<ClientStruct>();
        ArrayBlockingQueue<Message> messages
                = new ArrayBlockingQueue<Message>(100);
        Server server = new Server(clients, messages);
        Thread mainManager = new Thread(server);

        try{
            System.out.println("building SocketServer...");
            ServerSocket ss = new ServerSocket(5000);
            mainManager.start();
            while (true){
                System.out.println("waiting for clients to connect...");
                ClientStruct newClient = new ClientStruct(ss.accept(), messages
                        , clients.size());
                clients.add(newClient);
                System.out.println("new client connected");
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }
}
